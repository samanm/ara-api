import * as Koa from 'koa';
import * as HttpStatus from 'http-status-codes';
import userController from './user/user.controller';
import authController from './auth/auth.controller';
import pettyCashController from './pettyCash/petty-cash.controller';
import projectController from './project/project.controller';
import * as bodyParser from 'koa-bodyparser';

const cors: any = require('@koa/cors');
export default initializeApp();

function initializeApp() {
    const app: Koa = new Koa();

    app.use(cors({
        origin: '*',
        exposeHeaders: ['X-Total-Count', 'authorization'],
        allowMethods: 'GET,HEAD,PUT,POST,DELETE'
    }));
// Generic error handling middleware.
    app.use(async (ctx: Koa.Context, next: () => Promise<any>) => {
        try {
            await next();
        } catch (error) {
            ctx.status = error.statusCode || error.status || HttpStatus.INTERNAL_SERVER_ERROR;
            error.status = ctx.status;
            ctx.body = {error};
            ctx.app.emit('error', error, ctx);
        }
    });

    app.use(bodyParser());

    [
        userController,
        authController,
        pettyCashController,
        projectController
    ].forEach(controller => {
        app.use(controller.routes());
        app.use(controller.allowedMethods());
    });

// Initial route
//     app.use(async (ctx: Koa.Context) => {
//         ctx.body = 'Hello world';
//     });

// Application error logging.
    app.on('error', console.error);

    return app;
}


