import {Sequelize} from 'sequelize-typescript';
import User from '../user/user.model';
import {snakeCase} from 'lodash';
import {Parameter} from '../parameter';
import {emitEvent} from '../helpers/event.helper';
import {SequelizeHooks} from 'sequelize/types/lib/hooks';
import PettyCash from '../pettyCash/petty-cash.model';
import Project from '../project/project.model';

let instance: Storage;


const ALL_MODELS = [
    PettyCash,
    Project,
    User,
];

class Storage {
    public sequelize: Sequelize;
    public ready: boolean = false;

    constructor() {
        this.sequelize = new Sequelize(Parameter.DB_NAME, Parameter.DB_USER, Parameter.DB_PASSWORD, {
            dialect: 'postgres',
            host: Parameter.POSTGRES_HOST,
            logging: process.env.NODE_ENV === 'false',
            pool: {
                max: 10,
                acquire: 30000
            }
            // modelPaths: [path.join(__dirname, 'Models')]
        });
        this.sequelize.addModels(ALL_MODELS);
        this.sequelize.sync({force: false}).then(() => {
            this.ready = true;
            ALL_MODELS.forEach((model) => {
                ['afterCreate', 'afterUpdate', 'afterDestroy'].forEach((event: keyof SequelizeHooks) => {
                    model.addHook(event, (model: any) => {
                        emitEvent(this.generateEventName(model, event), model);
                    });
                });
            });
        });
    }

    private generateEventName(model: any, event: string) {
        return snakeCase(model._modelOptions.name.singular).toUpperCase() + '_' + event.replace('after', '').toUpperCase();
    }

    public onReady(callback: Function) {
        let _this = this;
        checkReady();

        function checkReady() {
            if (_this.ready) {
                callback();
            } else {
                setTimeout(checkReady, 1000);
            }
        }
    }
}

export function initializeStorage() {
    return new Promise((resolve) => {
        if (instance == null) {
            instance = new Storage();
        }
        instance.onReady(() => resolve(instance))
    });
}

export default instance;


