import {EventEmitter} from 'events';

const eventManager = new EventEmitter();

export function emitEvent(event: string, data?: any) {
    eventManager.emit(event.toString(), data);
}
