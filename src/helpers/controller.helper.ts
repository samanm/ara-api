import * as Koa from 'koa';
import * as jwt from 'jsonwebtoken';

import {Model} from 'sequelize-typescript';
import {intersection} from 'lodash';
import Router = require('koa-router');
import {API_VERSION} from '../config';
import {Op} from 'sequelize';
import {Role} from '../user/roles';
import {hasRole} from '../user/helpers';
import {Parameter} from '../parameter';


interface QueryString {
    _start: string;
    _end: string;
    _sort: string;
    _order: string;
    _attributes: string;
    _limit: string;
    _ids: string;
}

interface WhereField {
    validator?: (val: string) => Boolean;
    name: string;
}

interface IncludeField {
    model: any;
    as: string;
    attributes?: string[];
    where?: any;
}

export interface Route {
    handler: Function
    method?: string
    path?: string
    middleWares?: Function[]
    roles?: Role[]
}


export function generateListRouteHandler<M extends Model<M>>(model: any, query?: any) {
    return async (ctx: Koa.Context) => {
        const q = addNecessaryForeignKeys(query || await makeListQueryOptions(ctx, model), model);
        const {count, rows} = await model.findAndCountAll(q);
        ctx.set('X-Total-Count', count.toString());
        ctx.resource = rows;
        ctx.body = rows.map((r: Model) => r.get({plain: true}));
        ctx.status = 200;
    }
}

export function generateShowRouteHandler<M extends Model<M>>(model: any, query?: any) {
    return async (ctx: Koa.Context) => {
        console.log('qq0', makeShowQueryOptions(ctx, model));
        const q = addNecessaryForeignKeys(query || await makeShowQueryOptions(ctx, model), model);
        console.log('qq', q);
        ctx.resource = <M>await model.findOne(q);
        if (ctx.resource == null) {
            ctx.throw(404);
        }
        ctx.body = ctx.resource.get({plain: true});
    };
}

export function generateUpdateRouteHandler<M extends Model<M>>(model: any) {
    return async (ctx: Koa.Context) => {
        const queryOptions = await makeUpdateQueryOptions(ctx);
        await model.update(ctx.request.body, queryOptions);
        ctx.resource = await model.findOne({where: {id: ctx.params.id}});
        if (ctx.resource == null) {
            ctx.throw(404);
        }
        ctx.body = ctx.resource.get({plain: true});
        ctx.status = 200;
    }
}

export function generateDeleteRouteHandler<M extends Model<M>>(model: any) {
    return async (ctx: Koa.Context) => {
        const queryOptions = await makeRemoveQueryOptions(ctx);
        await model.destroy(queryOptions);
        ctx.status = 204;
    }
}

export function generateCreateRouteHandler<M extends Model<M>>(model: any, params: { prepareObject?: Function } = {}) {
    return async (ctx: Koa.Context) => {
        const queryOptions = await makeCreateQueryOptions(ctx);
        try {
            ctx.resource = await model.create((params.prepareObject ? await params.prepareObject(ctx) : ctx.request.body), queryOptions);
        } catch (e) {
            console.log(e);
            if (['SequelizeValidationError', 'SequelizeUniqueConstraintError'].indexOf(e.name) !== -1) {
                ctx.throw(400, e.errors.map((e: Error) => e.message).join(', '));
            } else {
                throw e;
            }
        }
        ctx.body = ctx.resource.get({plain: true});
        ctx.status = 201;
    }
}

export async function makeListQueryOptions(ctx: Koa.Context, model: any): Promise<any> {
    const {_start, _end, _sort, _order, _attributes, _limit, _ids}: QueryString = {...ctx.query} || {};
    let queryOptions: any = {
        where: {},
        include: [],
        order: [],
        associations: [],
    };
    if (!isNaN(parseInt(_start)) && !isNaN(parseInt(_end))) {
        queryOptions.offset = parseInt(_start);
        queryOptions.limit = Math.min(parseInt(_end) - queryOptions.offset, isAdmin(ctx) ? 1500 : 300);
    } else if (!isNaN(parseInt(_limit))) {
        if (!isNaN(parseInt(_start))) {
            queryOptions.offset = parseInt(_start);
        }
        queryOptions.limit = Math.min(parseInt(_limit), isAdmin(ctx) ? 1500 : 300);
    }
    if (queryOptions.limit == null) {
        queryOptions.limit = isAdmin(ctx) ? 1500 : 300;
    }
    if (_sort && Object.keys(model.rawAttributes).includes(_sort)) {
        queryOptions.order = [(['DESC', 'ASC'].includes(_order) ? [_sort, _order] : [_sort])];
    }
    if (_attributes) {
        queryOptions.attributes = intersection(_attributes.split(','), Object.keys(model.rawAttributes));
        queryOptions.associations = intersection(_attributes.split(','), Object.keys(model.associations));
    }
    if (_ids) {
        queryOptions.where = {
            ...(queryOptions.where as any),
            id: {[Op.in]: _ids.split(',').map(id => Number(id))}
        };

    }
    return queryOptions;
}

export async function makeShowQueryOptions(ctx: Koa.Context, model: any): Promise<any> {
    if (isNaN(ctx.params.id)) {
        ctx.throw(400);
    }
    let {_attributes} = ctx.query;
    let queryOptions: any = {
        where: {
            id: parseInt(ctx.params.id)
        },
        include: [],
        associations: [],
    };
    if (_attributes) {
        queryOptions.attributes = intersection(_attributes.split(','), Object.keys(model.rawAttributes));
        queryOptions.associations = intersection(_attributes.split(','), Object.keys(model.associations));
    }
    return queryOptions;
}

async function makeCreateQueryOptions(ctx: Koa.Context): Promise<any> {
    return {};
}

async function makeUpdateQueryOptions(ctx: Koa.Context) {
    if (isNaN(ctx.params.id)) {
        ctx.throw(400);
    }
    return {
        where: {id: <number>ctx.params.id},
        individualHooks: true
    };
}

async function makeRemoveQueryOptions(ctx: Koa.Context) {
    if (isNaN(ctx.params.id)) {
        ctx.throw(400);
    }
    return {
        where: {id: <number>ctx.params.id},
        individualHooks: true
    };
}

export function setupRoutes(router: Router, routes: Route[]) {
    routes.forEach(route => {
        // let route: Route = routeOptions.routes[routeName];
        const middleWares = route.middleWares || [];
        // let args = [buildHasAccessMiddleware(route.roles),...middleWares, route.handler];
        let args = [buildHasAccessMiddleware(route.roles), ...middleWares, route.handler];
        // if (route.roles && route.roles.length !== 0) {
        //     args = [this.buildHasAccessMiddleware(route.roles), ...args];
        // }
        // if (!route.disableJWT) {
        //     args = [jwtMiddleware, ...args];
        // }
        (router as any)[route.method](
            `/api/v${API_VERSION}` + route.path,
            ...args
        )
        ;
    });

}

function addNecessaryForeignKeys(queryOptions: any, model: any) {
    let {attributes, include} = queryOptions;
    if (include && attributes && model.associations) {
        for (let associationName in model.associations) {
            const association = model.associations[associationName];
            if (
                include.some((i: any) => i === associationName || i.as === associationName) &&
                association.options &&
                association.options.foreignKey &&
                attributes.indexOf(association.foreignKey) === -1 &&
                Object.keys(model.rawAttributes).indexOf(association.options.foreignKey.name) !== -1
            ) {
                attributes.push(association.foreignKey);
            }
        }
    }
    return {
        ...queryOptions,
        attributes
    };
}


export function addWhere(ctx: Koa.Context, fields: WhereField[], where: any = {}) {
    let w = {...where};
    fields.forEach((f: WhereField) => {
        const val = ctx.query[f.name];
        if (val != null && (f.validator == null || f.validator(val))) {
            w = {
                ...w,
                [f.name]: val
            };
        }
    });
    return w;
}

export function addSearchQuery(ctx: Koa.Context, fields: string[], where: any = {}) {
    const {searchQuery} = ctx.request.query;
    if (searchQuery) {
        where = {
            ...where,
            [Op.or]: fields.map(f => ({[f]: {[Op.iLike]: `%${searchQuery}%`}}))
        }
    }
    return where;
}

export function addInclude(ctx: Koa.Context, model: any, fields: IncludeField[], include: any = []) {
    if (ctx.query._attributes == null) {
        return include;
    }
    const associations = intersection(ctx.query._attributes.split(','), Object.keys(model.associations));
    let i = [...include];
    fields
        .filter(f => associations.includes(f.as))
        .forEach(f => {
            i.push(f);
        });

    return i;

}


export function ensureBodyField(ctx: Koa.Context, fields: string[]) {
    for (const field of fields) {
        if (ctx.request.body[field] == null) {
            ctx.throw(400, `${field.toUpperCase()}_MISSED`);
        }
    }
}

function buildHasAccessMiddleware(roles: Role[] = []) {
    return async function (ctx: Koa.Context, next: Function) {
        let token = resolveAuthorizationHeader(ctx);
        console.log('token', token);
        if (!token && roles.length === 0) {
            return next();
        }
        try {
            if (!Parameter.JWT_SECRET) {
                ctx.throw(401, 'Secret not provided');
            }
            ctx.state.user = await verify(token, Parameter.JWT_SECRET);
        } catch (e) {
            ctx.throw(401, e.message);
        }
        if (roles.length === 0) {
            return next();
        }
        console.log('token1', ctx.state.user);
        if (!ctx.state.user) {
            ctx.throw(403);
        }
        if (roles.includes(Role.ROLE_USER)) {
            return next();
        }
        for (const role of roles) {
            if (hasRole(role, ctx.state.user.roles)) {
                return next();
            }
        }
        ctx.throw(403);
    };
}


function verify(token: string, secret: string) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, secret, (error: Error, decoded: any) => {
            error ? reject(error.message === 'jwt expired' ? {
                ...error,
                message: 'AUTH_JWT_EXPIRED' // todo pick error from common package
            } : error) : resolve(decoded);
        });
    });
}

function resolveAuthorizationHeader(ctx: Koa.Context) {
    if (!ctx.header || !ctx.header.authorization) {
        return;
    }

    const parts = ctx.header.authorization.split(' ');

    if (parts.length === 2) {
        const scheme = parts[0];
        const credentials = parts[1];

        if (/^Bearer$/i.test(scheme)) {
            return credentials;
        }
    }
    ctx.throw(401, 'Bad Authorization header format. Format is "Authorization: Bearer <code>"');
}


function isAdmin(ctx: any) {
    return true;
}