import * as bcrypt from 'bcrypt';
import {
    Table,
    Column,
    Model,
    PrimaryKey,
    AllowNull,
    Unique,
    AutoIncrement, Default, DataType, BeforeCreate, BeforeUpdate, HasMany
} from 'sequelize-typescript';
import {Role} from './roles';
import {hasRole} from './helpers';
import PettyCash from '../pettyCash/petty-cash.model';
import Project from '../project/project.model';

@Table({timestamps: true})
export class User extends Model<User> {

    @PrimaryKey
    @AutoIncrement
    @Column
    id: number;

    @AllowNull(true)
    @Unique
    @Column
    email: string;

    @AllowNull(false)
    @Unique
    @Column
    phoneNumber: string;

    @AllowNull(false)
    @Column
    fullName: string;


    @AllowNull(true)
    @Column
    biography: string;

    @AllowNull(true)
    @Column
    password: string;


    @Default([])
    @Column(DataType.ARRAY(DataType.STRING))
    roles: Role[];


    @HasMany(() => PettyCash)
    pettyCashes: PettyCash[];


    @HasMany(() => Project)
    projectPettyCashiers: Project[];

    hasRole(role: Role) {
        return hasRole(role, this.roles);
    }

    @BeforeCreate
    static hashPassword(user: User, options: any) {
        if (user.password) {
            return bcrypt.hash(user.password, 5).then((hash: string) => {
                user.password = hash;
            });
        }
    }

    @BeforeUpdate
    static updateHash(user: User, options: any) {
        if (user.changed('password')) {
            return bcrypt.hash(user.password, 5).then((hash: string) => {
                user.password = hash;
            });
        }
    }


    static comparePassword(plainPass: string, hashedPass: string): Promise<{}> {
        return bcrypt.compare(plainPass, hashedPass);
    }
}

export default User
