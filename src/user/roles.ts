export enum Role {
    ROLE_ADMIN = 'ROLE_ADMIN',
    ROLE_SUPERVISOR = 'ROLE_SUPERVISOR',
    ROLE_USER = 'ROLE_USER'
}

export const RoleTree = {
    ROLE_ADMIN: [Role.ROLE_SUPERVISOR],
};
