import {Role, RoleTree} from './roles';

export function hasRole(role: Role, roles: Role[]) {
    if (roles == null) {
        return false;
    }
    for (const r of roles) {
        if (containRole(r, role)) {
            return true;
        }
    }
    return false;

    function containRole(sourceRole: Role, targetRole: Role) {
        if (sourceRole === targetRole) {
            return true;
        }
        if (RoleTree.hasOwnProperty(sourceRole)) {
            for (const r of (RoleTree as any)[sourceRole.toString()]) {
                if (containRole(r, targetRole)) {
                    return true;
                }
            }
        }
        return false;
    }

}