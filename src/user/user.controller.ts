import * as Router from 'koa-router';
import * as Koa from 'koa';

import {
    addWhere,
    generateCreateRouteHandler,
    generateDeleteRouteHandler,
    generateListRouteHandler,
    generateShowRouteHandler,
    generateUpdateRouteHandler,
    makeListQueryOptions,
    Route,
    setupRoutes
} from '../helpers/controller.helper';
import UserModel from './user.model';
import {Role} from './roles';
import User from './user.model';

const router: Router = new Router();

const routes: Route[] = [
    {
        handler: listHandler,
        method: 'get',
        path: '/user/'
    },
    {
        handler: showHandler,
        method: 'get',
        path: '/user/:id'
    },
    {
        handler: createHandler,
        method: 'post',
        path: '/user/',
    },
    {
        handler: generateUpdateRouteHandler<UserModel>(UserModel),
        method: 'put',
        path: '/user/:id'
    },
    {
        handler: generateDeleteRouteHandler<UserModel>(UserModel),
        method: 'delete',
        path: '/user/:id'
    },
];

async function listHandler(ctx: Koa.Context) {
    let {where, ...query} = await makeListQueryOptions(ctx, UserModel);
    where = addWhere(ctx, [
        {name: 'fullName'},
        {name: 'email'},
        {name: 'phoneNumber'},
    ], where);
    const handler = generateListRouteHandler(UserModel, {where, ...query});
    await handler(ctx);
    ctx.body = ctx.body.map((u: any) => ({...u, password: undefined}));
}

async function showHandler(ctx: Koa.Context) {
    const handler = generateShowRouteHandler(User);
    await handler(ctx);
    delete ctx.body.password;
}


async function createHandler(ctx: Koa.Context) {
    const handler = generateCreateRouteHandler(User);
    await handler(ctx);
    delete ctx.body.password;
}

setupRoutes(router, routes);
export default router;