require('dotenv').config();

export class Parameter {
    static get DB_NAME() {
        return process.env.DB_NAME;
    }

    static get DB_PASSWORD() {
        return process.env.DB_PASSWORD;
    }

    static get DB_USER() {
        return process.env.DB_USER;
    }

    static get POSTGRES_HOST() {
        return process.env.POSTGRES_HOST;
    }

    static get JWT_SECRET(){
        return process.env.JWT_SECRET;
    }
    static get SERVER_PORT(){
        return process.env.SERVER_PORT;
    }
}



