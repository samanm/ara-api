import app from './app';
import {initializeStorage} from './database';
import {Parameter} from './parameter';
// import databaseConnection from './database/database.connection';

const PORT: number = Number(Parameter.SERVER_PORT) || 3000;

initializeStorage().then(() => {
    app.listen(PORT)
});
