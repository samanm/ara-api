import {
    Table,
    Column,
    Model,
    PrimaryKey,
    AllowNull,
    AutoIncrement, HasMany, ForeignKey, Default
} from 'sequelize-typescript';
import PettyCash from '../pettyCash/petty-cash.model';
import User from '../user/user.model';
import PROJECT_STATUS from './project-status.enum';

@Table({timestamps: true})
export class Project extends Model<Project> {

    @PrimaryKey
    @AutoIncrement
    @Column
    id: number;

    @AllowNull(false)
    @Column
    title: string;

    @AllowNull(true)
    @Column
    description: string;

    @AllowNull(false)
    @Default(PROJECT_STATUS.NOT_STARTED)
    @Column
    state: PROJECT_STATUS;

    @HasMany(() => PettyCash)
    pettyCashes: PettyCash[];


    @ForeignKey(() => User)
    @AllowNull(true)
    @Column
    pettyCashierId: number;

}

export default Project
