import * as Router from 'koa-router';
import * as Koa from 'koa';

import {
    addSearchQuery,
    addWhere,
    generateCreateRouteHandler,
    generateDeleteRouteHandler,
    generateListRouteHandler,
    generateShowRouteHandler,
    generateUpdateRouteHandler,
    makeListQueryOptions,
    Route,
    setupRoutes
} from '../helpers/controller.helper';
import Project from './project.model';
import {Role} from '../user/roles';
import {isNumeric} from '../helpers/validator.helper';

const router: Router = new Router();

const routes: Route[] = [
    {
        handler: listHandler,
        method: 'get',
        path: '/project/',

    },
    {
        handler: generateShowRouteHandler<Project>(Project),
        method: 'get',
        path: '/project/:id'
    },
    {
        handler: generateCreateRouteHandler<Project>(Project),
        method: 'post',
        path: '/project/',
    },
    {
        handler: generateUpdateRouteHandler<Project>(Project),
        method: 'put',
        path: '/project/:id'
    },
    {
        handler: generateDeleteRouteHandler<Project>(Project),
        method: 'delete',
        path: '/project/:id'
    },
];

async function listHandler(ctx: Koa.Context) {
    let {where, ...query} = await makeListQueryOptions(ctx, Project);
    where = addWhere(ctx, [
        {name: 'pettyCashierId', validator: isNumeric},
        {name: 'state'},
        {name: 'title'},
    ], where);
    where = addSearchQuery(ctx, ['title', 'description'], where);
    const handler = generateListRouteHandler(Project, {where, ...query});
    await handler(ctx);
}

setupRoutes(router, routes);
export default router;