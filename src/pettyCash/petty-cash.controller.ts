import * as Router from 'koa-router';
import * as Koa from 'koa';

import {
    addSearchQuery,
    addWhere,
    generateCreateRouteHandler,
    generateDeleteRouteHandler,
    generateListRouteHandler,
    generateShowRouteHandler,
    generateUpdateRouteHandler,
    makeListQueryOptions,
    Route,
    setupRoutes
} from '../helpers/controller.helper';
import PettyCash from './petty-cash.model';
import {Role} from '../user/roles';
import Project from '../project/project.model';
import {isNumeric} from '../helpers/validator.helper';

const router: Router = new Router();

const routes: Route[] = [
    {
        handler: listHandler,
        method: 'get',
        path: '/pettyCash/'
    },
    {
        handler: generateShowRouteHandler<PettyCash>(PettyCash),
        method: 'get',
        path: '/pettyCash/:id'
    },
    {
        handler: generateCreateRouteHandler<PettyCash>(PettyCash),
        method: 'post',
        path: '/pettyCash/',
    },
    {
        handler: generateUpdateRouteHandler<PettyCash>(PettyCash),
        method: 'put',
        path: '/pettyCash/:id'
    },
    {
        handler: generateDeleteRouteHandler<PettyCash>(PettyCash),
        method: 'delete',
        path: '/pettyCash/:id'
    },
];


async function listHandler(ctx: Koa.Context) {
    let {where, ...query} = await makeListQueryOptions(ctx, PettyCash);
    where = addWhere(ctx, [
        {name: 'amount', validator: isNumeric},
        {name: 'projectId', validator: isNumeric},
        {name: 'employeeId', validator: isNumeric},
        {name: 'title'}
    ], where);
    where = addSearchQuery(ctx, ['title', 'note'], where);
    const handler = generateListRouteHandler(PettyCash, {where, ...query});
    await handler(ctx);
}

setupRoutes(router, routes);
export default router;