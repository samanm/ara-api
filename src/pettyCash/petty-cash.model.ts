import {
    Table,
    Column,
    Model,
    PrimaryKey,
    AllowNull,
    AutoIncrement,ForeignKey, BelongsTo
} from 'sequelize-typescript';
import Project from '../project/project.model';
import User from '../user/user.model';

@Table({timestamps: true})
export class PettyCash extends Model<PettyCash> {

    @PrimaryKey
    @AutoIncrement
    @Column
    id: number;

    @AllowNull(false)
    @Column
    title: string;

    @AllowNull(false)
    @Column
    amount: number;

    @AllowNull(true)
    @Column
    note: string;

    @ForeignKey(() => Project)
    @AllowNull(true)
    @Column
    projectId: number;

    @BelongsTo(() => Project, 'projectId')
    project: Project;


    @ForeignKey(() => User)
    @AllowNull(true)
    @Column
    employeeId: number;

    @BelongsTo(() => User, 'employeeId')
    employee: User;

}

export default PettyCash
