import * as Router from 'koa-router';
import * as Koa from 'koa';
import * as jwt from 'jsonwebtoken';
import {
    ensureBodyField,
    Route, setupRoutes
} from '../helpers/controller.helper';
import User from '../user/user.model';
import {Parameter} from '../parameter';

const router: Router = new Router();

const routes: Route[] = [
    {
        handler: loginHandler,
        method: 'post',
        path: '/auth/login',
    }
];

async function loginHandler(ctx: Koa.Context) {
    ensureBodyField(ctx, ['phoneNumber', 'password']);
    const {phoneNumber, password} = ctx.request.body;
    let user = await User.findOne<User>({
        where: {phoneNumber}
    });
    if (user == null) {
        ctx.throw(400, 'AUTH_INVALID_CREDENTIALS')
    }
    let isMatch = await User.comparePassword(password, user.password);
    if (!isMatch) {
        ctx.throw(400, 'AUTH_INVALID_CREDENTIALS');
    }

    const token = jwt.sign(await userPayload(user), Parameter.JWT_SECRET, {expiresIn: '2h'});
    ctx.body = {token};
}

async function userPayload(user: User) {
    return {
        id: user.id,
        roles: user.roles,
        phoneNumber: user.phoneNumber
    }
}

setupRoutes(router, routes);

export default router;