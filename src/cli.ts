import * as program from 'commander';
import {initializeStorage} from './database';
import User from './user/user.model';
import {Role} from './user/roles';
const {prompt} = require('inquirer');


let cmdValue: any;

program
    .version('0.0.1')
    .description('Baladshow Cli')
    .action(function (cmd, env) {
        cmdValue = cmd;
    });

program
    .command('user:create')
    .description('Create User')
    .action(() => {
        prompt([
            {
                type: 'input',
                name: 'fullName',
                message: 'Enter Full Name:'
            }, {
                type: 'input',
                name: 'phoneNumber',
                message: 'Enter Phone Number:'
            }, {
                type: 'input',
                name: 'password',
                message: 'Enter password:'
            }, {
                type: 'confirm',
                name: 'admin',
                message: 'is Admin ?'
            }
        ]).then((answer: any) => {
            initializeStorage().then(storage => {
                User.create({
                    fullName: answer.fullName,
                    phoneNumber: answer.phoneNumber,
                    password: answer.password,
                    roles: answer.admin ? [Role.ROLE_ADMIN] : [],
                }).then(() => {
                    console.log('User Created Successfully');
                    process.exit();
                });
            });
        });
    });


program
    .command('user:change-password')
    .description('Changing user password')
    .action(() => {
        prompt([
            {
                type: 'input',
                name: 'email',
                message: 'Enter Email'
            }, {
                type: 'input',
                name: 'password',
                message: 'Enter new password:'
            }
        ]).then((answer: any) => {
            initializeStorage().then(storage => {
                User.findOne({where: {email: answer.email}}).then((user:User) => {
                    if (user == null) {
                        console.log('User Not Found');
                        process.exit();
                    } else {
                        user.password = answer.password;
                        user.save().then(() => {
                            console.log('Password changed Successfully.');
                            process.exit();
                        });
                    }
                });
            });
        });
    });




program
    .command('*')
    .action(function (env) {
        program.help();
    });

program.parse(process.argv);

